<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Models\Company;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\Response;

class CompanyController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        return JsonResource::collection(Company::all());
    }

    public function show(string $id): JsonResource|Response
    {
        try {
            return new JsonResource(Company::findOrFail($id));
        } catch (Exception) {
            return response(null, Response::HTTP_NOT_FOUND);
        }
    }

    public function store(StoreCompanyRequest $request): JsonResource
    {
        $company = Company::create($request->all());
        return new JsonResource($company);
    }

    public function update(UpdateCompanyRequest $request, Company $company): JsonResource
    {
        $company->update($request->all());

        return new JsonResource($company);
    }

    public function destroy(Company $company): Response
    {
        $company->delete();

        return response(null, Response::HTTP_OK);
    }
}
