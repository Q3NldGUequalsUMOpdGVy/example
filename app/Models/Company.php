<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $registration_number
 * @property Carbon $founded_at
 * @property string $country
 * @property string $zipcode
 * @property string $city
 * @property string $address
 * @property float $latitude
 * @property float $longitude
 * @property string $owner_name
 * @property int $employee_count
 * @property string $activity
 * @property bool $is_active
 * @property string $email
 * @property string $password
 */
class Company extends Model {

    use HasFactory;

    protected $table = 'companies';

    protected $fillable = [
        'name',
        'registration_number',
        'founded_at',
        'country',
        'zipcode',
        'city',
        'address',
        'latitude',
        'longitude',
        'owner_name',
        'employee_count',
        'activity',
        'is_active',
        'email',
        'password',
    ];

    protected $casts = [
        'founded_at' => 'date:Y-m-d',
        'latitude' => 'float',
        'longitude' => 'float',
        'employee_count' => 'int',
        'is_active' => 'boolean',
    ];
}
