<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class StoreCompanyRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'registration_number' => ['required', 'regex:/^\d{6}-\d{4}$/'],
            'founded_at' => ['required', 'date', 'date_format:Y-m-d'],
            'country' => ['required', 'string'],
            'zipcode' => ['required', 'string'],
            'city' => ['required', 'string'],
            'address' => ['required', 'string'],
            'latitude' => ['required', 'numeric', 'between:-90,90'],
            'longitude' => ['required', 'numeric', 'between:-180,180'],
            'owner_name' => ['required', 'string'],
            'employee_count' => ['required', 'integer', 'min:0'],
            'activity' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
            'email' => ['required', 'email'],
            'password' => ['required', Password::min(5)]
        ];
    }
}
