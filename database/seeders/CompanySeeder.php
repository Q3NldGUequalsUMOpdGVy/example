<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class CompanySeeder extends Seeder
{
    public function run(): void
    {
        $fileContent = explode(PHP_EOL, Storage::disk('local')->get('testCompanyDB.csv'));
        array_shift($fileContent); //headers
        array_pop($fileContent); //new line

        foreach ($fileContent as $row) {
            $companyAttributes = str_getcsv($row, ';');
            array_shift($companyAttributes); //ID
            $companyAttributes = static::mapAttributesFromCSV($companyAttributes);
            Company::updateOrCreate(
                [
                    'registration_number' => $companyAttributes['registration_number'],
                ],
                $companyAttributes
            );
        }
    }

    private static function mapAttributesFromCSV(array $input): array
    {
        $fillableAttributes = app(Company::class)->getFillable();
        $attributes = [];

        foreach ($input as $key => $value) {
            $value = match ($fillableAttributes[$key]) {
                'founded_at' => Carbon::createFromFormat('Y.m.d', $value),
                'is_active' => filter_var($value, FILTER_VALIDATE_BOOLEAN),
                'password' => Hash::make($value),
                default => $value,
            };

            $attributes[$fillableAttributes[$key]] = $value;
        }

        return $attributes;
    }
}
