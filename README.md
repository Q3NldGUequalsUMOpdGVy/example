A megoldás során a feladatokhoz mérten arányos megoldásokra törekedtem, természetesen lehetne több validationt írni,
JsonResourceos megoldás helyett kézzel írni a methodokat, indexelni az oszlopokat, stb.

Apró eltérés a feladatleíráshoz képest, hogy a legfrissebb Laravelt használtam így PHP 7 helyett 8-at használ a projekt.
Nem teljesen egyértelmű, de feltételeztem, hogy a created_at lenne a regisztráció ideje. Ha ez mégis a
companyFoundationDate,
akkor értelemszerűen ez a triggerben és a form validatorokban, valamint a model fillable tömbjében lenne lekövetve. :)

Setup:

1. ```./vendor/bin/sail up -d```
2. Erre a pontra a db szintű update tiltás miatt van szükség:
    - ```./vendor/bin/sail shell```
    - ```mysql -h mysql -u root -p``` (password .env fileban: ```password```)
    - ```GRANT SUPER ON *.* TO 'sail'@'%' WITH GRANT OPTION;```
3. ```./vendor/bin/sail artisan migrate```
4. ```./vendor/bin/sail db:seed```
5. ```companies.postman_collection.json``` file importálható Postmanbe, alap CRUD műveleteket + indexet tartalmazza.
6. ```./vendor/bin/sail phpunit``` futtatja az egyszerű feature teszteket.

SQL queryk:

``` sql
SELECT 
  GROUP_CONCAT(
    DISTINCT CONCAT(
      'MAX(CASE WHEN activity = ''', activity, 
      ''' THEN name END) AS `', activity, 
      '`'
    )
  ) INTO @sql 
FROM 
  companies;
  
SET @sql = CONCAT('SELECT ', @sql, ' FROM companies GROUP BY name');
  
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
```

``` sql
SET 
  SESSION cte_max_recursion_depth = 1000000;
WITH RECURSIVE date_ranges AS (
  SELECT 
    '2001-01-01' AS date 
  UNION ALL
  SELECT 
    date + INTERVAL 1 DAY 
  FROM 
    date_ranges 
  WHERE 
    date < CURRENT_DATE
)

SELECT date, name 
FROM date_ranges 
LEFT JOIN companies ON date_ranges.date = companies.founded_at;
```
