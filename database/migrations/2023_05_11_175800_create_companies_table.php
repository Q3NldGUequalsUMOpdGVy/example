<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('companies', static function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('registration_number');
            $table->date('founded_at');
            $table->string('country');
            $table->string('zipcode');
            $table->string('city');
            $table->string('address');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('owner_name');
            $table->integer('employee_count');
            $table->string('activity');
            $table->boolean('is_active');
            $table->string('email');
            $table->string('password');
            $table->timestampsTz();
        });
        // must give sail super privileges before running this
        DB::unprepared('
            CREATE TRIGGER prevent_update_trigger
            BEFORE UPDATE ON companies
            FOR EACH ROW
            BEGIN
                IF NEW.created_at != OLD.created_at THEN
                    SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "Updating created_at is not allowed.";
                END IF;
            END
        ');
        // remove super privileges from sail if possible after
    }

    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
