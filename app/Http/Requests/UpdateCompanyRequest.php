<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UpdateCompanyRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string'],
            'registration_number' => ['regex:/^\d{6}-\d{4}$/'],
            'founded_at' => ['date', 'date_format:Y-m-d'],
            'country' => ['string'],
            'zipcode' => ['string'],
            'city' => ['string'],
            'address' => ['string'],
            'latitude' => ['numeric', 'between:-90,90'],
            'longitude' => ['numeric', 'between:-180,180'],
            'owner_name' => ['string'],
            'employee_count' => ['integer', 'min:0'],
            'activity' => ['string'],
            'is_active' => ['boolean'],
            'email' => ['email'],
            'password' => [Password::min(5)]
        ];
    }
}
